# Web app Calculatrice

Le but était de développer une application web proposant une calculatrice simple (les 4 opérations de base).  
Il devait être possible ensuite d'historiser les résultats d'un calcul dans un fichier .txt, et d'afficher cet historique.   

Cependant, quelques fonctionnalités supplémentaires ont été ajoutés.  

## **Fonctionnalités supplémentaires**

- Gestion des nombres à virgule
- Ajout des parenthèses ouvrantes et fermantes pour écrire des expressions avec des parenthèses
- Ajout d'un bouton "retour arrière" pour supprimer le dernier caractère du calcul
- **Contrôle avancé des erreurs d'écriture du calcul par l'utilisateur, empêchant l'écriture de tout calcul syntaxiquement invalide**

## **Parti pris adopté**

Après avoir ajouté différentes fonctionnalités, j'ai décidé d'orienter le développement de la calculatrice autour de l'expérience utilisateur.  

L'utilisateur est donc pris au maximum par la main dans l'écriture de son calcul pour qu'il lui soit impossible de taper un calcul syntaxiquement impossible. Pour ce faire, le dernier caractère tapé du calcul est constamment analysé pour désactiver tous les boutons qui pourraient rendre le calcul invalide s-il était appuyé (par exemple, après avoir appuyé sur "\*", on désactive les boutons "\*", "/" et ")" car ces successions de symbole mènerait à un calcul syntaxiquement incorrect. On désactive aussi le bouton "=" car aucun calcul ne se termine par un "*").

Ainsi, il est par exemple :

- impossible de taper deux symboles "\*" ou "/" fois d'affilée
- possible de taper "+-" ou -+" mais impossible de taper 3 opérateurs ou plus d'affilée 
- impossible d'écrire un nombre avec deux virgules
- impossible de fermer une parenthèse juste après l'avoir ouverte, ou de fermer une parenthèse juste après un opérateur
- impossible d'ouvrir une parenthèse juste après un chiffre, une virgule ou après une parenthèse fermante
- impossible de commencer un calcul par un "\*", un "/" ou par une parenthèse fermante
- impossible de valider un calcul (appuyer sur le bouton "=" pour voir le résultat) si le calcul est mal parenthésé
- impossible de valider un calcul s'il se termine par un opérateur ou par une parenthèse ouvrante
- ...

De manière générale, il n'est pas censé être possible de taper et de valider (appuyé sur "=") un calcul invalide qui mènerait donc à un résultat soulevant l'erreur. Toutefois, si cela arrive, ou si le code HTML a été modifié intentionnellement et manuellement pour enlever l'attribut "disabled" d'un bouton désactivé pour écrire un mauvais calcul, la validation du calcul affichera "ERREUR!" à la place du résultat et le bouton "historiser" ne s'activera pas.  

Encore une fois, si de manière intentionnelle le code HTML est changé pour activer le bouton d'historisation pour historiser un résultat ayant soulevé une erreur, ou pour historiser un calcul alors que l'input calcul et/ou l'input résultat est vide, des vérifications sont faites en php pour empêcher l'historisation de tout calcul incohérent. Un message d'erreur sera alors affiché à l'utilisateur lui indiquant que le calcul n'a pas été historisé.

## **Comment installer et faire fonctionner l'application ?**

Pour lancer l'application et interagir avec la calculatrice, il suffit tout d'abord de cloner le projet avec git en lançant cette commande dans un dossier quelconque à partir d'une invite de commandes :

```bash
git clone https://gitlab.com/Simiah/web-app-calculatrice.git
```

Il suffira ensuite de lancer cette commande php dans le dossier créé pour lancer l'application web :

```bash
php -S localhost:8000
```

Votre application est lancée, il suffit maintenant de vous rendre sur votre navigateur à l'adresse "localhost:8000" pour interagir avec la calculatrice.

*Note : A titre indicatif, cette application a été développé à partir de la version 7.4.1 de php*