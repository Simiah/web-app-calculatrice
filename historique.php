<html>
    <head>
        <meta charset="utf-8" />
        <title>L'historique - Web app Calculatrice</title>
        <link href="style.css?<?php echo time(); ?>" type="text/css" rel="stylesheet">
        <link rel="shortcut icon" href="#"/>
    </head>
    <body>
    <h1>L'historique des résultats</h1>
    <?php
        //On vérifie d'abord si le fichier d'historique existe et qu'il n'est pas vide. Dans le cas contraire, on affiche un message d'erreur à la place du tableau
        if(!file_exists("historique.txt") || filesize('historique.txt') == 0) {
            echo "<p class=\"warning\">Aucun historique disponible :'(</p>";
        }
        else {
        ?>
        <table>
            <thead>
                <th>N° du calcul</th>
                <th>Calcul</th>
                <th>Résultat</th>
            </thead>
            <tbody>
                <?php

                    $fileHisto = fopen('historique.txt', 'r'); //On ouvre le fichier en lecture seul
                    $nbCalculation=1;

                    while($line = fgets($fileHisto)) { //Pour chaque ligne du fichier d'historisation, on explose le calcul qui est sur la ligne à partir du "=", pour afficher le calcul et le résultat dans une case différente
                        $calculation = explode("=", $line);
                        ?>

                        <tr>
                            <td><?=$nbCalculation?></td>
                            <td><?=$calculation[0]?></td>
                            <td><?=$calculation[1]?></td>
                        </tr>

                        <?php
                        $nbCalculation++;
                    }

                    fclose($fileHisto);

                ?>
            </tbody>
        </table>
    <?php } ?>
    <p><a href="index.php">Retourner à la calculatrice ?</a></p>
    </body>
</html>