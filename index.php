<html>
    <head>
        <meta charset="utf-8" />
        <title>La calculatrice - Web app Calculatrice</title>
        <link href="style.css?<?php echo time(); ?>" type="text/css" rel="stylesheet">
        <link rel="shortcut icon" href="#">
        <script src="javascript.js"></script>
    </head>
    <body>
        <h1>La calculatrice</h1>
        <div id="calculator">
            <form action="index.php" method="post">
                <div>
                    <input type=text name="calculation" id="calculation" placeholder="calcul" readonly/>
                    <button type="button" name="btn-transferResult" id="btn-transferResult" value="<" onclick="transferResult()"><</button>
                    <input type=text name="result" id="result" placeholder="résultat" readonly/>
                </div>
                <div class="btns btns-number">
                    <button type="button" name="btn-1" id="btn-1" value="1" onclick="refreshCalculation('1')">1</button>
                    <button type="button" name="btn-2" id="btn-2" value="2" onclick="refreshCalculation('2')">2</button>
                    <button type="button" name="btn-3" id="btn-3" value="3" onclick="refreshCalculation('3')">3</button>
                    <button type="button" name="btn-4" id="btn-4" value="4" onclick="refreshCalculation('4')">4</button>
                    <button type="button" name="btn-5" id="btn-5" value="5" onclick="refreshCalculation('5')">5</button>
                    <button type="button" name="btn-6" id="btn-6" value="6" onclick="refreshCalculation('6')">6</button>
                    <button type="button" name="btn-7" id="btn-7" value="7" onclick="refreshCalculation('7')">7</button>
                    <button type="button" name="btn-8" id="btn-8" value="8" onclick="refreshCalculation('8')">8</button>
                    <button type="button" name="btn-9" id="btn-9" value="9" onclick="refreshCalculation('9')">9</button>
                    <button type="button" name="btn-0" id="btn-0" value="0" onclick="refreshCalculation('0')">0</button>
                    <button type="button" name="btn-dot" id="btn-dot" value="." onclick="refreshCalculation('.')">.</button>
                </div>
                <div class="btns btns-operator">
                    <button type="button" name="btn-add" id="btn-add" disabled onclick="refreshCalculation('+')">+</button>
                    <button type="button" name="btn-sub" id="btn-sub" disabled onclick="refreshCalculation('-')">-</button>
                    <button type="button" class="btn-toDisable" name="btn-mul" id="btn-mul" disabled onclick="refreshCalculation('*')">*</button>
                    <button type="button" class="btn-toDisable" name="btn-div" id="btn-div" disabled onclick="refreshCalculation('/')">/</button>
                    <button type="button" class="btn-toDisable" name="btn-eql" id="btn-eql" disabled onclick="calculate()">=</button>
                </div>
                <div class="btns btns-parenthesis">
                    <button type="button" name="btn-leftPar" id="btn-leftPar" onclick="refreshCalculation('(')">(</button>
                    <button type="button" name="btn-rightPar" id="btn-rightPar" disabled onclick="refreshCalculation(')')">)</button>
                </div>
                <div>
                    <input type="submit" name="btn-submit" id="btn-submit" value="historiser" disabled/>
                    <input type="reset" name="btn-reset" id="btn-reset" value="effacer" onclick="resetBtns()"/>
                    <button type="button" name="btn-return" id="btn-return" value="retour" onclick="deleteLastCharacter()"><<</button>

                    <input type="hidden" id="haveResultError" name="haveResultError" value="true"/>
                </div>
            </form>
        </div>
        <p><a href="historique.php">Voir l'historique des calculs ?</a></p>  
        <?php

            if(isset($_POST["calculation"]) && isset($_POST["result"]) && isset($_POST["haveResultError"])) { //On vérifie que le formulaire a bien été envoyé
                //On vérifie que l'input hidden indiquant si il y a une erreur dans le résultat n'est pas égal à true
                //Par sécurité, on vérifie aussi que l'input texte du calcul et du résultat n'est pas vide, et que le résultat n'indique pas "ERREUR!" ou "DIVISION PAR 0!", car le html peut être manipulé pour mettre la valeur de "haveResultError" à false manuellement
                if($_POST["haveResultError"] == "false" && $_POST["result"] != "ERREUR!" && $_POST["result"] != "DIVISION PAR 0!" && $_POST["result"] != "" && $_POST["calculation"] != "") {
                    $fileHisto = fopen('historique.txt', 'a'); //On ouvre le fichier en écriture seule. Le pointeur est placé à la fin du fichier pour écrire à la fin du fichier, et le fichier est crée si il n'existe pas
                    fputs($fileHisto, $_POST["calculation"]."=".$_POST["result"]."\n"); // On écrit le calcul et le résultat séparé d'un "="
                    fclose($fileHisto); 

                    echo "<p class=\"warning\">Votre calcul a bien été historisé !</p>"; //On averti l'utilisateur de l'historisation
                }
                else echo "<p class=\"warning\">Erreur dans le résultat : calcul non-historisé !</p>"; //On averti l'utilisateur de la présence d'une erreur, et donc que le calcul ne sera pas historisé
            }

        ?>
    </body>
</html>