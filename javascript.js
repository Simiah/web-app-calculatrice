//L'input texte recevant le calcul
var calculationArea;
//L'input texte recevant le resultat
var resultArea;

//Variable qui compte le nombre de signe d'opération successif
var nbSuccessiveOperator = 0;

//Variable qui est égal à true si le nombre qui se trouve à la fin du calcul est un nombre décimal
//Elle est indispensable pour désactiver le bouton "." lorsqu'on manipule déjà un nombre décimal pour éviter de taper un nombre avec deux virgules
var haveDot = false;

//Permet de remettre la calculette à l'état d'origine lors du chargement/rechargement de la page
window.onload = function() {
    calculationArea = document.getElementById("calculation");
    resultArea = document.getElementById("result");

    //On vide les inputs de calcul et de résultat, et on les mets en readonly par sécurité pour pouvoir écrire un calcul qu'avec les boutons
    calculationArea.readonly = true;
    calculationArea.value="";

    resultArea.readonly = true;
    resultArea.value="";

    resetBtns();
}

//Cette fonction permet de remettre les boutons dans leur état de base (activé/désactivé) lorsque la zone de calcul est vide
//Un calcul ne pas commencer par une parenthèse droite, ou par un signe multiplé ou diviser
function resetBtns() {
    disableButtons(".btns-operator .btn-toDisable, #btn-submit, #btn-rightPar, #btn-transferResult");
    enableButtons(".btns-number button, .btns-operator button:not(.btn-toDisable), #btn-leftPar");
    nbSuccessiveOperator = 0;
    haveDot=false;
}

//Cette fonctione met à jour l'input texte du calcul avec le caractère à ajouter selon le bouton qui a été appuyé
//Elle met aussi à jour l'état des boutons (activer/désactiver) selon le caractère ajouté (pour éviter d'écrire un calcul impossible par la suite)
function refreshCalculation(characterToAdd) {

    calculationArea.value = calculationArea.value+characterToAdd;
    //On vide l'input texte du résultat si il n'est pas vide
    //Comme la fonction "refreshCalculation()" modifie l'input texte du calcul et donc le calcul, on vide l'input du résultat pour éviter une incohérence entre le calcul affiché et le résultat affiché
    resultArea.value = "";

    document.getElementById("haveResultError").value = true // On remet la valeur de l'input indiquant si il y a une erreur à true, car le calcul vient d'être changé 

    changeDisableState(characterToAdd);
    
    calculationArea.focus(); //Permet de mettre le curseur à la fin de l'input texte du calcul, dans le cas où le calcul est plus grand que la largeur de l'input

}

//Cette fonctione supprime le dernier caractère de l'input texte du calcul
//Elle est appelé lorsqu'on appuie sur le bouton "retour arrière"
function deleteLastCharacter() {

    //On vide l'input texte du résultat si il n'est pas vide
    //Comme la fonction "deleteLastCharacter()" modifie l'input texte du calcul et donc le calcul, on vide l'input du résultat pour éviter une incohérence entre le calcul affiché et le résultat affiché
    resultArea.value = "";

    document.getElementById("haveResultError").value = true // On remet la valeur de l'input indiquant si il y a une erreur à true, car le calcul vient d'être changé 

    let characterDeleted = getLastCharacter(calculationArea.value); //On récupère le caractère qui va être supprimé

    calculationArea.value=calculationArea.value.substring(0, calculationArea.value.length-1); //On supprime le dernier caractère
    changeDisableState(getLastCharacter(calculationArea.value), characterDeleted); //On change l'état des boutons selon le nouveau dernier caractère, car un caractère a été supprimé

}

/*
    Cette fonction permet de changer l'état des boutons (activer/désactiver) selon le caractère se trouvant à la fin de l'input texte du calcul
    Elle permet d'empêcher au maximum à l'utilisateur d'entrer un calcul impossible, qui indiquerait une erreur lors du résultat

    Le paramètre "characterDeleted" contient le caractère qui vient d'être supprimé dans le cas d'une suppresion. Sinon il ne contient rien

*/
function changeDisableState(character, characterDeleted = "") {

    if(isAnOperator(characterDeleted)) {
        //Si le caractère supprimé est un opérateur, alors on décrémente le nombre d'opérateur d'affilée. Comme un opérateur est supprimé, on vérifie également qu'il n'y a pas un nombre décimal à la fin de l'input, pour activer ou non le bouton virgule
        haveDot = checkIfLastNumberIsADecimalNumber();
        nbSuccessiveOperator--;
    }
    else if(characterDeleted == ".") haveDot = false; //Une virgule a été supprimé, le nombre à la fin de l'input n'est donc plus un nombre décimal

    if(isAnOperator(character)) { //Si le caractère à la fin de l'input est un opérateur
        if(characterDeleted == "") { //Dans le cas où aucun caractère supprimé (on vient d'appuyer sur un bouton pour ajouter un caractère)
            //On vient d'ajouter un opérateur, alors on incrémente la variable, et en ajoutant un opérateur on met aussi fin au nombre que l'on était en train d'écrire. Alors haveDot passe à false
            haveDot = false;
            nbSuccessiveOperator++;
        }
        else nbSuccessiveOperator = checkNbSuccessiveOperator(); //Si un caractère a été supprimé, et qu'on est dans le cas où le dernier caractère de l'input est un opérateur, on récupère le nombre d'opérateur successif à la fin de l'input

        //Quoiqu'il arrive, après un opérateur il peut y avoir un chiffre, une virgule, une parenthèse ouvrante mais pas une parenthèse fermante
        enableButtons(".btns-number button, #btn-leftPar");
        disableButtons("#btn-RightPar");

        if(nbSuccessiveOperator<2) { // Si il y a moins de deux opérateurs d'affilée
            if(character == "+") {
                //Après un "+", il est possible de mettre un "-" mais pas n'importe quel autre opérateur
                disableButtons(".btns-operator button:not(#btn-sub)");
                enableButtons(".btns-operator #btn-sub"); 
            }
            else if(character == "-") {
                //Après un "-", il est possible de mettre un "+" mais pas n'importe quel autre opérateur
                disableButtons(".btns-operator button:not(#btn-add)");
                enableButtons(".btns-operator #btn-add");
            }
            else {
                //Sinon, c'est que l'opérateur est un "*" ou "/", et il est impossible d'avoir n'importe quel autre opérateur après ces types d'opérateur
                disableButtons(".btns-operator .btn-toDisable");
                enableButtons(".btns-operator button:not(.btn-toDisable)");
            }
        }
        else {
            //Sinon, il y a deux opérateurs d'affilée ("+-" ou "-+"), et il est impossible d'avoir 3 opérateurs d'affilée
            disableButtons(".btns-operator button");
        }
    }
    else { // Dans le cas où ce n'est pas un opérateur
        if(character == ".") {
            //Après une virgule, il ne peut pas y avoir un autre virgule ou une parenthèse ouvrante. Il peut en revanche avoir un chiffre, un opérateur ou une parenthèse fermante
            disableButtons(".btns-number #btn-dot, #btn-leftPar");
            enableButtons(".btns-number button:not(#btn-dot), .btns-operator button, #btn-rightPar");

            haveDot = true; //On vient d'ajouter une virgule. On indique donc que le nombre se trouvant à la fin de l'input est un nombre décimal et qu'il ne peut pas avoir plusieurs virgules

            checkIfParenthesisAreValid(); //Un calcul peut se terminer par une virgule. Avant d'activer le bouton "=", on vérifie que la calcul est bien parenthésé
        }
        else if(character == "(") {
            //Après une parenthèse ouvrante, on ne pas mettre un "*" ou "/", ni une parenthèse fermante, mais il est possible de mettre une autre parenthèse ouvrante, un chiffre, une virgule ou un "+" ou "-"
            disableButtons(".btns-operator .btn-toDisable, #btn-rightPar");
            enableButtons(".btn-number button, .btns-operator button:not(.btn-toDisable), #btn-leftPar");
        }
        else if(character == ")") {
            //Après une parenthèse fermante, On ne peut pas mettre un chiffre, une virgule ou une parenthèse ouvrante, mais on peut mettre un opérateur ou une parenthèse fermante
            disableButtons(".btns-number button, #btn-leftPar");
            enableButtons(".btns-operator button, #btn-rightPar");

            checkIfParenthesisAreValid(); //Un calcul peut se terminer par une parenthèse fermante. Avant d'activer le bouton "=", on vérifie que la calcul est bien parenthésé
        }
        else if(character =="") resetBtns(); //Si le dernier caractère de l'input texte du calcul est le caractère vide, alors l'input est vide. On remet donc tous les boutons à leur état d'origine
        else {
            //Sinon, le caractère à la fin est un chiffre. Après un chiffre il peut y avoir n'importe quoi à part une parenthèse ouvrante: Un autre chiffre, un opérateur, une virgule ou une parenthèse fermante
            disableButtons("#btn-leftPar");
            enableButtons(".btns button:not(#btn-leftPar)");

            checkIfParenthesisAreValid(); //Un calcul peut se terminer par un chiffre. Avant d'activer le bouton "=", on vérifie que la calcul est bien parenthésé
        }
        nbSuccessiveOperator=0; //Le caractère ajouté n'est pas un opérateur, alors on réinitilise la variable "nbSuccessiveOperator"
    }

    if(haveDot) disableButtons(".btns-number #btn-dot"); //Si le nombre à la fin de l'input est un nombre décimal, alors un désactive le bouton virgule
    //On désactive le bouton submit, pour éviter de pouvoir historiser un calcul tant que l'utilisateur n'a pas appuyé sur "="
    //On désactive aussi le bouton qui permet de transférer le résultat dans la zone de calcul : Le calcul vient d'être modifié et donc l'input du résultat a été vidé.
    disableButtons("#btn-submit, #btn-transferResult"); 

}

//Cette fonction permet de désactiver tous les boutons sélectionnés par le sélecteur en argument
function disableButtons(selector) {
    let btnToDisabled = document.querySelectorAll(selector);
    Array.from(btnToDisabled).forEach(button => {
        button.disabled = true;
    });
}

//Cette fonction permet d'activer tous les boutons sélectionnés par le sélecteur en argument
function enableButtons(selector) {
    let btnToActivate = document.querySelectorAll(selector);
    Array.from(btnToActivate).forEach(button => {
        button.disabled = false;
    });
}

//Cette fonction exécute le calcul dans l'input texte du calcul et affiche le résultat dans l'input texte du résultat
function calculate() {

    try {
        //On exécute le calcul
        let result = eval(calculationArea.value);

        if(result=="Infinity") throw dividedByZeroError(); //Si result est égal à "Infinity", c'est qu'il y a eut une division par zéro

        resultArea.value = result; //On affiche le résultat
        //On active le bouton submit pour pouvoir historiser le calcul et le résultat
        //On active aussi le bouton de transfert de résultat pour pouvoir transférer le résultat fraichement calculé dans l'input du calcul si on le souhaite
        enableButtons("#btn-submit, #btn-transferResult"); 
        document.getElementById("haveResultError").value = false; //On change la valeur de l'input hidden pour indiquer qu'il n'y a pas d'erreur
    }
    catch(e) {
        //Dans le cas où une erreur a été soulevé lors de l'exécution de eval()...

        if(e.name == "dividedByZeroError") resultArea.value = "DIVISION PAR 0!" //Si il y a eut une division par zéro, on averti l'utilisateur
        else resultArea.value = "ERREUR!"; //On affiche dans l'input texte du résultat qu'il y a une erreur

        //On désactive le bouton submit pour éviter d'historiser un calcul avec une erreur
        //On désactive aussi le bouton de transfert de résultat pour éviter de transférer un résultat avec une erreur
        disableButtons("#btn-submit, #btn-transferResult"); 
        document.getElementById("haveResultError").value = true; //On change la valeur de l'input hidden pour indiquer qu'il y a une erreur
    }
    
}

//Cette fonction retourne true si le nombre à la fin de l'input texte du calcul est un nombre décimal
function checkIfLastNumberIsADecimalNumber() {

    let characters = calculationArea.value.split(''); //On récupère tous les caractères du calcul
    let currentCharacter = characters.length-1;  // On récupère le dernier caractère du calcul pour partir de la fin de la chaine de caractère

    /*
        On part de la fin de la chaine de caractère du calcul
        On s'arrête lorsque :
            - On a parcouru toute la chaine
            OU
            - On a trouvé un opérateur (Cela veut dire qu'on a finit de parcourir le nombre se trouvant à la fin de l'input texte du calcul)
        Si avant de sortir de la boucle while, on tombe sur une virgule ".", on return true, sinon on return false
    */
    while(!isAnOperator(characters[currentCharacter]) && currentCharacter>=0) {
        if(characters[currentCharacter] == ".") return true;
        currentCharacter--;
    }

    return false;
}


//Cette fonction vérifie si le calcul est bien parenthésé
//Elle est appelé avant d'activer le bouton "=", pour vérifier que le calcul est bien parenthésé avant d'autoriser l'utilisateur d'exécuter le calcul est levé une erreur
function checkIfParenthesisAreValid() {

    let characters = calculationArea.value.split(''); //On récupère tous les caractères du calcul
    let cpt = 0;

    for(let i = 0 ; i<characters.length ; i++) {
        if(characters[i] == "(") cpt++;
        else if(characters[i] == ")") cpt--;

        //On sort de la boucle si cpt est négatif, car cela veut dire qu'une parenthèse a été fermé alors qu'elle n'a jamais été ouverte
        if(cpt<0) break;
    }

    //Si cpt!=0, alors le calcul est mal parenthésé, et on désactive donc le bouton "égal" pour ne pas créer une erreur lors du résultat
    if(cpt!=0) disableButtons("#btn-eql");

}

//Cette fonction retourne le nombre d'opérateur successif à la fin de l'input du calcul
function checkNbSuccessiveOperator() {

    let characters = calculationArea.value.split(''); //On récupère tous les caractères du calcul
    let currentCharacter = characters.length-1; // On récupère le dernier caractère du calcul pour partir de la fin de la chaine de caractère

    let nbSuccessiveOperator=0;

    // On part de la fin de la chaine de caractère du calcul et on incrémente le nombre d'opérateur successif tant qu'on ne tombe pas sur autre chose qu'un opérateur
    while(isAnOperator(characters[currentCharacter])) {
        currentCharacter--;
        nbSuccessiveOperator++; 
    }

    return nbSuccessiveOperator;

}

//Cette fonction retourne true si le character en argument est un opérateur
function isAnOperator(character) {
    return ["+", "-", "*", "/"].includes(character);
}

//Cette fonction retourne le dernier caractère de la chaine en argument
function getLastCharacter(string) {
    return string.substring(string.length-1, string.length);
}

//Cette fonction permet de transférer le résultat dans l'input texte du calcul
function transferResult() {

    calculationArea.value = resultArea.value; //On récupère la valeur du résultat pour le mettre dans l'input texte du calcul
    resultArea.value = ""; //On vide l'input texte du résultat

    //On change l'état des boutons selon le dernier caractère du résultat
    changeDisableState(getLastCharacter(calculationArea.value));

    //On vérifie sur le résultat (qui se trouve maintenant dans l'input du calcul) est un nombre décimal pour désactiver ou non le bouton virgule en consésquence, et pour avertir qu'on travaille sur un nombre décimal
    if(isADecimalNumber(calculationArea.value)) {
        haveDot = true;
        disableButtons("#btn-dot");
    }

}

//Cette fonction retourne true si le nombre en argument est un nombre décimal (si il contient un ".")
function isADecimalNumber(string) {
    return string.includes('.');
}

function dividedByZeroError() {
    throw {
      name: 'dividedByZeroError',
      message: 'Division by zero occured'
    };
  }